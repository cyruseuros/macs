;;; containers+.el -*- lexical-binding: t; -*-

(use-package kubernetes
  :general
  (kbd+ "k" '(nil :wk "container"))
  (kbd+ :infix "k"
    "k" '(kubernetes-overview :wk "kubernetes")))

(use-package kubernetes-evil
  :after kubernetes evil)

(use-package dockerfile-mode
  :defer t)

(provide 'containers+)
;;; containers+.el ends here
