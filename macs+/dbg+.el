;;; dbg+.el -*- lexical-binding: t; -*-

(setq gdb-many-windows t)

(provide 'dbg+)
;;; dbg+.el ends here
