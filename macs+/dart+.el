;;; dart+.el -*- lexical-binding: t; -*-

(use-package dart-mode
  :defer t
  :config (after+ 'projectile
            (list+ 'projectile-project-root-files-bottom-up "pubspec.yaml")
            (list+ 'projectile-project-root-files-bottom-up "BUILD")))

(provide 'dart+)
;;; dart+.el ends here
