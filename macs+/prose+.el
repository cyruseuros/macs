;;; prose+.el -*- lexical-binding: t; -*-

(use-package mw-thesaurus
  :general
  (kbd+ "xt"
    '(mw-thesaurus-lookup-at-point
      :wk "definition")))

(provide 'prose+)
;;; prose+.el ends here
