;;; build+.el -*- lexical-binding: t; -*-

(use-package imake
  :commands imake)
(use-package meson-mode
  :defer t)

(list+ 'auto-mode-alist
       '("\\.wrap\\'" . conf-toml-mode))

(after+ 'all-the-icons
  (add-to-list
   'all-the-icons-icon-alist
   '("^makefile$" all-the-icons-fileicon
     "gnu" :face all-the-icons-dorange)))


(provide 'build+)
;;; build+.el ends here
