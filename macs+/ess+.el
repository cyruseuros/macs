;;; ess+.el -*- lexical-binding: t; -*-

;;; vars
(modes+ 'ess+ #'ess-mode #'inferior-ess-mode)

;;; packages
(use-package ess
  :init
  (setq ess-use-ido nil
        ess-use-flymake
        (not (featurep 'flycheck)))
  (setq-default ess-indent-with-fancy-comments nil)
  (hook+ 'ess-help-mode-hook
               #'evil-motion-state)
  :config
  (after+ 'helm-company
    (advice+ #'ess-indent-or-complete
             :after-until #'helm-company))
  (after+ 'ess-r-mode
    (syntax+ #'inferior-ess-r-mode
             ?% "$%"
             ?' "$'"))
  :general
  (general-def :keymaps 'ess-mode-map
    [M-return] #'ess-eval-line-and-step)
  (general-def :keymaps 'inferior-ess-r-mode-map
    "M-n" #'comint-next-input
    "M-p" #'comint-previous-input)
  (kbd+local
    :keymaps ess+maps
    "," '(ess-eval-region-or-function-or-paragraph-and-step
          :wk "eval dwim")
    "p" '(ess-switch-process
          :wk "switch process")))

;;; calls
(after+ 'completion+
  (compdef
   :modes ess+modes
   :company '(company-capf
              company-dabbrev-code
              company-yasnippet)
   :capf '(ess-r-object-completion
           ess-filename-completion
           dabbrev-completion
           t)))

(after+ 'handle
  (handle ess+modes
          :evaluators #'ess-eval-buffer
          :evaluators-line #'ess-eval-line
          :evaluators-selection #'ess-eval-region
          :formatters #'indent-region
          :docs #'ess-display-help-on-object
          :gotos #'ess-display-help-on-object
          :repls #'ess-switch-to-inferior-or-script-buffer))

(provide 'ess+)
;;; ess+.el ends here
