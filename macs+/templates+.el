;;; templates+.el -*- lexical-binding: t; -*-

(use-package yatemplate
  :ghook ('prog-mode-hook #'yatemplate-fill-alist)
  :init (setq yatemplate-dir (dirs+templates+)))

(provide 'templates+)
;;; templates+.el ends here
