;;; emacs+.el -*- lexical-binding: t; -*-

;;; vars
(setq enable-recursive-minibuffers t
      epa-pinentry-mode 'loopback
      vc-follow-symlinks t
      help-window-select t)

;;; funs
(defun emacs+yank nil
  "Copy the current buffer's path to the kill ring."
  (interactive)
  (if-let* ((filename
             (or buffer-file-name
                 (bound-and-true-p list-buffers-directory))))
      (message (kill-new (abbreviate-file-name filename)))
    (error "Couldn't find filename in current buffer.")))

(defun emacs+frame nil
  "Open current window in new frame closing it."
  (interactive)
  (and (display-buffer-other-frame
        (current-buffer))
       (previous-buffer)))

(defun emacs+macs+ nil
  "Search .emacs.d files."
  (interactive)
  (helm-find-files-1
   (dirs+macs+)))

;;; packages
(use-package crux
  :commands crux-with-region-or-buffer
  :general
  (general-def
    [C-backspace] #'crux-kill-line-backwards)
  (kbd+
    :infix "f"
    "i" '(crux-find-user-init-file :wk "init file")
    "z" '(crux-find-shell-init-file :wk "zsh init")
    "o" '(crux-open-with :wk "open external")
    "n" '(crux-rename-file-and-buffer :wk "rename file")
    "d" '(crux-delete-file-and-buffer :wk "delete file")
    "e" '(crux-sudo-edit :wk "sudo edit")))

(use-package helpful
  :general
  (kbd+
    :infix "h"
    "v" '(helpful-variable :wk "variable")
    "m" '(describe-mode :wk "mode")
    "f" '(helpful-callable :wk "function")
    "k" '(helpful-key :wk "key")))

(use-package no-littering
  :config
  (after+ 'recentf
    (list+ 'recentf-exclude no-littering-var-directory)
    (list+ 'recentf-exclude no-littering-etc-directory))
  (setq custom-file (no-littering-expand-etc-file-name "custom.el"))
  (setq auto-save-file-name-transforms
        `((".*" ,(no-littering-expand-var-file-name "autosave/") t))))

(use-package exec-path-from-shell
  :init
  (setq exec-path-from-shell-variables
        '("PATH" "MANPATH"
          "GOPATH" "PYTHONPATH"
          "NODE_PATH" "QT_QPA_PLATFORMTHEME")
        exec-path-from-shell-check-startup-files nil)
  :config (exec-path-from-shell-initialize))

(use-package direnv
  :config (direnv-mode)
  (list+ 'direnv-non-file-modes
         #'vterm-mode))

(use-package pcre2el
  :config (pcre-mode +1))
(use-package smooth-scrolling
  :config (smooth-scrolling-mode +1))
(use-package restart-emacs
  :general (kbd+ "qr" '(restart-emacs :wk "restart emacs")))

;;; calls
(defalias #'yes-or-no-p #'y-or-n-p)
(advice+ #'crux-open-with :after
         (lambda (&rest _)
           (unless (derived-mode-p 'dired-mode)
             (previous-buffer))))

(kbd+
  "oi" #'ielm
  "fy" '(emacs+yank :wk "yank filename")
  "of" '(emacs+frame :wk "open frame")
  "ou" '(undo-tree-visualize :wk "undo tree"))
(after+ 'helm+ (kbd+ "fm" '(emacs+macs+ :wk "macs files")))

(provide 'emacs+)
;;; emacs+.el ends here
