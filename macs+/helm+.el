;;; helm+.el -*- lexical-binding: t; -*-

(setq helm+sandbox "~/sandbox/"
      helm+fuzzy
      ;; (if (executable-find "rustc")
      ;;     'rust 'elisp)
      ;; 'elisp
      ;; 'rust
      ;; nil
      t
      )

;;; funs
(defun helm+faces nil
  "Describe face with `helm'."
  (interactive)
  (require 'helm-elisp)
  (let ((default (or (face-at-point) (thing-at-point 'symbol))))
    (helm :sources (helm-def-source--emacs-faces
                    (format "%s" (or default "default")))
          :buffer "*helm faces*")))

(defun helm+sandbox nil
  "Got to sandbox directory"
  (interactive)
  (helm-find-files-1 helm+sandbox))

(defun helm+ag-dir nil
  (interactive)
  (helm-do-ag (file-name-directory buffer-file-name)))

(use-package helm
  :init
  (setq helm-show-completion-display-function nil
        helm-display-buffer-default-height 0.35
        helm-default-display-buffer-functions
        '(display-buffer-in-side-window))
  :config
  (helm-mode +1)
  (when helm+fuzzy
    (setq helm-ff-fuzzy-matching t
          helm-buffers-fuzzy-matching t
          helm-M-x-fuzzy-match t
          helm-etags-fuzzy-match t
          helm-locate-fuzzy-match t
          helm-apropos-fuzzy-match t
          helm-recentf-fuzzy-match t
          helm-session-fuzzy-match t
          helm-file-cache-fuzzy-match t
          helm-locate-library-fuzzy-match t
          helm-completion-style 'emacs
          completion-styles
          (if (version< emacs-version "27")
              '(helm-flex)
            '(flex))))
  (advice+ #'helm-locate-update-mode-line
           :override #'ignore)
  :general
  (general-def [remap execute-extended-command] #'helm-M-x)
  (general-def :keymaps 'helm-map :package 'helm
    [tab] #'helm-execute-persistent-action
    [backtab] #'helm-select-action)
  (kbd+
    "y" '(helm-show-kill-ring :wk "kill ring")
    "SPC" '(helm-M-x :wk "extended command")
    kbd+localleader '(helm-resume :wk "resume action")
    "ss" '(helm-occur :wk "search buffer")
    "bb" '(helm-mini :wk "list buffers"))
  (kbd+ :infix "j"
    "j" '(helm-imenu :wk "jump")
    "o" '(helm-imenu-in-all-buffers :wk "jump other"))
  (kbd+ :infix "f"
    "f" '(helm-find-files :wk "find file")
    "h" '(helm-recentf :wk "file history")
    "r" '(helm-find :wk "find recursive")
    "l" '(helm-locate :wk "locate file")))

(use-package helm-rg
  :disabled t
  :general
  (kbd+local :keymaps 'helm-rg--bounce-mode-map
    "," '(helm-rg--bounce-dump :wk "commit edits")
    "r" '(helm-rg--bounce-refresh :wk "revert edits"))
  (kbd+ :keymaps 'helm-rg-map
    "," '(helm-rg--bounce :wk "edit search"))
  (kbd+ :infix "s"
    "d" '(helm-rg :wk "search directory")
    "p" '(helm-projectile-rg :wk "search project")))

(use-package helm-ag
  :init
  (setq-default
   helm-ag-insert-at-point 'symbol)
  :config
  (when helm+fuzzy (setq helm-ag-fuzzy-match t))
  (advice+ #'helm-ag-show-status-default-mode-line
           :override #'ignore)
  :general
  (kbd+local :keymaps 'helm-ag-edit-map
    "," '(helm-ag--edit-commit :wk "commit edits")
    "c" '(helm-ag--edit-abort :wk "cancel edits"))
  (kbd+ :keymaps 'helm-ag-map
    "e" '(helm-ag-edit :wk "edit search"))
  (kbd+ :infix "s"
    "f" '(helm-do-ag-this-file :wk "search file")
    "p" '(helm-do-ag-project-root :wk "search project")
    "b" '(helm-do-ag-buffers :wk "search buffers")))

(use-package helm-system-packages
  :general (kbd+ "ha" '(helm-system-packages :wk "arch")))

;;; fuzzy
(use-package fuz
  :config
  (unless (require 'fuz-core nil t)
    (fuz-build-and-load-dymod))
  :if (eq helm+fuzzy 'rust))
(use-package helm-fuz
  :if (eq helm+fuzzy 'rust)
  :config (helm-fuz-mode +1)
  :after fuz)
(use-package helm-fuzzier
  :config (helm-fuzzier-mode +1)
  :if (eq helm+fuzzy 'elisp))
(use-package helm-flx
  :init
  (setq helm-flx-for-helm-find-files t
        helm-flx-for-helm-locate t)
  :config (helm-flx-mode +1)
  :if (eq helm+fuzzy 'elisp))


;;; calls
(kbd+
  "hi" '(helm-info :wk "helm info")
  "hs" '(helm+faces :wk "search faces")
  "fb" '(helm+sandbox :wk "find sandbox")
  "sd" '(helm+ag-dir :wk "search directory"))

(provide 'helm+)
;;; helm+.el ends here
