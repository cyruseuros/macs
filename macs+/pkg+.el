;;; pkg+.el -*- lexical-binding: t; -*-

;;; package
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("gnu" . "https://elpa.gnu.org/packages/")
        ("org" . "https://orgmode.org/elpa/")))

;;; straight
(setq straight-repository-branch "develop"
      straight-use-package-by-default t
      straight-recipes-emacsmirror-use-mirror t
      straight-recipes-gnu-elpa-use-mirror t
      straight-vc-git-default-clone-depth 1
      straight-check-for-modifications '(check-on-save))
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (switch-to-buffer (messages-buffer))
    (with-current-buffer
        (url-retrieve-synchronously
         (concat "https://raw.githubusercontent.com/"
                 "raxod502/straight.el/develop/install.el")
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-x-process-limit 25)
(require 'straight-x)

(straight-use-package 'use-package)
(use-package benchmark-init
  :if init-file-debug)

(provide 'pkg+)
;;; pkg+.el ends here
