;;; nim+.el -*- lexical-binding: t; -*-

(use-package nim-mode
  :defer t
  :config
  (after+ 'compdef
    (compdef
     :modes #'nim-mode
     :company #'company-nimsuggest)))

(provide 'nim+)
;;; nim+.el ends here
