;;; transient+.el -*- lexical-binding: t; -*-

(use-package which-key
  :init
  (setq which-key-compute-remaps t
        which-key-allow-evil-operators t
        which-key-side-window-max-height 0.4
        which-key-allow-multiple-replacements t
        which-key-sort-order 'which-key-prefix-then-key-order)
  :general (kbd+ "hw" #'which-key-show-top-level)
  :config (which-key-mode +1))

(use-package hercules
  :straight (hercules
             :type git
             :host gitlab
             :branch "develop"
             :repo "jjzmajic/hercules.el"
             :depth full))

(provide 'transient+)
;;; transient+.el ends here
