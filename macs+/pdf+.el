;;; pdf+.el -*- lexical-binding: t; -*-

;;; funs
(defun pdf+grep-file ()
  (interactive)
  (helm-do-pdfgrep-1
   `(,buffer-file-truename) nil))

(defun pdf+grep-dir ()
  (interactive)
  (helm-do-pdfgrep-1
   (directory-files
    (file-name-directory buffer-file-truename)
    ".*.pdf")
   nil))

(defun pdf+grep-recurse ()
  (interactive)
  (helm-do-pdfgrep-1
   (directory-files-recursively
    (file-name-directory buffer-file-truename)
    ".*.pdf")
   t))

;;; packages
(use-package pdf-view-restore
  :ghook 'pdf-view-mode-hook)
(use-package pdf-tools
  :init (pdf-loader-install)
  :defer t)

;;; calls
(after+ 'helm
  (kbd+local
    :keymaps '(doc-view-mode-map
               pdf-view-mode-map)
    "s" '(:wk "search")
    "sf" '(pdf+grep-file :wk "file")
    "sd" '(pdf+grep-dir :wk "directory")
    "sr" '(pdf+grep-recurse :wk "recursive")))

(provide 'pdf+)
;;; pdf+.el ends here
