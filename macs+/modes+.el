;;; modes+.el -*- lexical-binding: t; -*-

(defun derive+ (modes mmh)
  (cl-loop for mode in modes
           collect (pcase mmh
                     ('modes modes)
                     ('maps (derived-mode-map-name mode))
                     ('hooks (derived-mode-hook-name mode)))))

(defun modes+ (prefix &rest modes)
  (require 'derived)
  (let ((prefix (symbol-name prefix))
        (suffixes '("modes" "maps" "hooks")))
    (dolist (suffix suffixes)
      (set (intern (concat prefix suffix))
           (pcase suffix
             ("modes" modes)
             ("maps" (derive+ modes 'maps))
             ("hooks" (derive+ modes 'hooks)))))))

(defun syntax+ (&optional mode &rest chars)
  "More convenient form of `modify-syntax-entry'."
  (when chars
    (modify-syntax-entry
     (pop chars) (pop chars)
     (symbol-value (derived-mode-syntax-table-name mode)))
    (apply #'syntax+ mode chars)))

(use-package guess-style
  :straight
  (guess-style
   :type git
   :host github
   :repo "nschum/guess-style")
  :config (global-guess-style-info-mode +1)
  :defer t)

(use-package handle
  :straight
  (handle
   :type git
   :host gitlab
   :repo "jjzmajic/handle")
  :commands handle
  :init
  (setq handle-keywords
        '(
          ;; +++
          :compilers
          :docs
          :errors
          :evaluators
          :evaluators-line
          :evaluators-paragraph
          :evaluators-selection
          :formatters
          :gotos
          :repls
          ;; +++
          ))
  :general
  (kbd+
    :infix "c"
    "k" '(handle-docs :wk "doc")
    "g" '(handle-gotos :wk "goto")
    "r" '(handle-repls :wk "repl")
    "f" '(handle-formatters :wk "format")
    "e" '(handle-evaluators :wk "eval")
    "l" '(handle-evaluators-line :wk "eval line")
    "p" '(handle-evaluators-paragraph :wk "eval paragraph")
    "s" '(handle-evaluators-selection :wk "eval selection")
    "c" '(handle-compilers :wk "compile")))

(use-package quickrun
  :general
  (kbd+ "hq"
    '(helm-quickrun
      :wk "helm quickrun")))

(use-package webpaste
  :general (kbd+ "fp" #'webpaste-paste-region)
  :config (after+ 'crux (crux-with-region-or-buffer
                         webpaste-paste-region)))
(use-package string-inflection :defer t)


(hook+ 'prog-mode-hook
       '(column-number-mode
         hs-minor-mode))
(handle '(prog-mode text-mode)
        :gotos #'dumb-jump-go
        :compilers #'imake
        :formatters #'indent-region
        :evaluators #'quickrun)

(provide 'modes+)
;;; modes+.el ends here
