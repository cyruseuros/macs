;;; lint+.el -*- lexical-binding: t; -*-

;;; vars
(setq lint+textlintrc
      (let ((textlintrc (dirs+.config "textlint/" "textlintrc.json")))
        (if (file-exists-p textlintrc) textlintrc
          (dirs+config+ "textlintrc.json"))))

;;; funs
(defun code+toggle-flycheck nil
  "Toggle flycheck's error list window."
  (interactive)
  (let ((window (flycheck-get-error-list-window)))
    (if window
        (quit-window nil window)
      (flycheck-list-errors))))

;;; packages
(use-package flycheck
  :commands (flycheck-get-error-list-window)
  :init
  (setq flycheck-standard-error-navigation t
        flycheck-display-errors-function nil
        flycheck-checker-error-threshold 1000
        flycheck-indication-mode nil
        flycheck-textlint-config lint+textlintrc)
  :ghook 'prog-mode-hook
  :config (flycheck-add-next-checker 'tex-chktex 'textlint)
  :general (kbd+
             "cx" '(code+toggle-flycheck :wk "errors")
             "tf" '(flycheck-mode :wk "flycheck")))

(use-package flyspell
  :hook ((prog-mode . flyspell-prog-mode)
         (text-mode . flyspell-mode)))


(use-package flyspell-correct-helm
  :general (general-def :states 'normal
             "z=" #'flyspell-correct-wrapper)
  :after helm)

(provide 'lint+)
;;; lint+.el -*- lexical-binding: t; -*-
