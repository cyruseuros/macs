;;; vhdl+.el -*- lexical-binding: t; -*-

(use-package vhdl-capf
  :defer t)

(after+ 'compdef
  (compdef :modes #'vhdl-mode
           :capf #'vhdl-capf-main
           :company completion+fallback))

(provide 'vhdl+)
;;; vhdl+.el ends here
