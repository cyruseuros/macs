;;; nav+.el -*- lexical-binding: t; -*-

(use-package semantic
  :gfhook ('prog-mode-hook
           '(semantic-mode
             which-function-mode))
  :init (list+
         'semantic-default-submodes
         #'global-semantic-stickyfunc-mode)
  :config (list+
           'semantic-new-buffer-setup-functions
           '(emacs-lisp-mode . semantic-default-elisp-setup)))

(use-package dumb-jump
  :init (after+ 'helm (setq dumb-jump-selector 'helm))
  :defer t)

(provide 'nav+)
;;; nav+.el ends here
