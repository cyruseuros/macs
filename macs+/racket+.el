;;; racket+.el -*- lexical-binding: t; -*-

(modes+ 'racket+ #'racket-mode #'racket-repl-mode)

;; TODO: implement this [[https://www.racket-mode.com/#Install]]
(use-package racket-mode
  :general
  (kbd+local :keymaps racket+maps
    kbd+localleader #'racket-send-last-sexp)
  :gfhook #'racket-eldoc-function
  :config
  (after+ 'handle
    (handle racket+modes
	    :repls #'racket-repl
	    :evaluators #'racket-run))
  (after+ 'compdef
    (compdef :modes racket+modes
             :company completion+fallback
             :capf #'racket-complete-at-point)))

;; TODO: implement pollen support
(use-package pollen-mode
  :disabled t
  :defer t)

(provide 'racket+)
;;; racket+.el ends here
