;;; elisp+.el -*- lexical-binding: t; -*-

;;; packages
(use-package macrostep
  :config
  (after+ 'transient+
    (hercules-def
     :toggle-funs #'macrostep-mode
     :keymap 'macrostep-keymap))
  :general
  (kbd+local
    :keymaps 'emacs-lisp-mode-map
    "m" #'macrostep-expand))

(use-package edebug
  :defer t
  :config
  (after+ 'transient+
    (hercules-def
     :show-funs '(edebug-mode)
     :keymap 'edebug-mode-map
     :transient t)))

(use-package flycheck-package
  :after flycheck
  :config (flycheck-package-setup))

(use-package highlight-quoted
  :hook (emacs-lisp-mode . highlight-quoted-mode))

(kbd+local
  :keymaps 'emacs-lisp-mode-map
  "," #'eval-last-sexp
  "e" '(:ignore t :wk "eval"))

(kbd+local
  :infix "e"
  :keymaps 'emacs-lisp-mode-map
  "d" #'eval-defun
  "r" #'eval-region)

;;; calls
(after+ 'compdef
  (compdef
   :modes '(emacs-lisp-mode inferior-emacs-lisp-mode)
   :company `(company-elisp
              ,completion+fallback)
   :capf (if (featurep 'company)
             #'elisp-completion-at-point
           #'helm-lisp-completion-or-file-name-at-point)))

(after+ 'handle
  (handle '(emacs-lisp-mode helpful-mode help-mode)
          :formatters #'indent-region
          :gotos '(find-function-at-point
                   dumb-jump-go-other-window)
          :docs #'helpful-at-point
          :repls #'ielm
          :evaluators #'eval-buffer
          :compilers #'byte-compile-file))

(provide 'elisp+)
;;; elisp+.el ends here
