;;; python+.el -*- lexical-binding: t; -*-

(modes+ 'python+ #'python-mode #'jupyter-repl-mode)

(defun python+send-paragraph ()
  (interactive)
  (when-let* ((bounds (bounds-of-thing-at-point 'sentence)))
    (cl-destructuring-bind (beg . end) bounds
      (jupyter-eval-region beg end)
      (and (forward-thing 'sentence +2)
           (forward-thing 'sentence -1)))))

(use-package pipenv
  :ghook 'python-mode-hook
  :init (setq pipenv-projectile-after-switch-function
              #'pipenv-projectile-after-switch-default)
  :config (advice+ #'pipenv-activate :before-while
                   (apply-partially #'jupyter-available-kernelspecs t)))

(use-package jupyter
  :init (setq jupyter-repl-echo-eval-p t)
  :commands (jupyter-completion-at-point
             jupyter-available-kernelspecs)
  :general (kbd+local :keymaps python+maps
             kbd+localleader #'python+send-paragraph
             "p" '(nil :wk "pipenv"))
  (kbd+local :keymaps python+maps
    :infix "p"
    "i" #'pipenv-install
    "a" #'pipenv-activate
    "d" #'pipenv-deactivate)
  :init
  (let ((background (ewal-get-color 'background))
        (foreground (ewal-get-color 'foreground))
        (green (ewal-get-color 'green))
        (red (ewal-get-color 'red)))
    (rice+
     `(jupyter-repl-traceback
       :background ,red
       :foreground ,foreground)
     `(jupyter-repl-input-prompt
       :background ,background
       :foreground ,green)
     `(jupyter-repl-output-prompt
       :background ,background
       :foreground ,red))))

(after+ 'handle
  (handle python+modes
          :repls #'jupyter-run-repl
          :evaluators #'jupyter-eval-buffer
          :evaluators-line #'jupyter-eval-line-or-region
          :evaluators-selection #'jupyter-eval-line-or-region
          :docs '(jupyter-inspect-at-point
                  lsp-describe-thing-at-point)))

(after+ 'lsp+
  (hook+ 'python-mode-hook
         #'lsp-deferred))

(after+ 'compdef
  (compdef
   :modes #'python-mode
   :company `(company-lsp ,completion+fallback)
   :capf #'jupyter-completion-at-point)
  (compdef
   :modes #'jupyter-repl-mode
   :capf #'jupyter-completion-at-point
   :company completion+fallback))

(provide 'python+)
;;; python+.el ends here
