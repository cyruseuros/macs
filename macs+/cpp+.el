;;; cpp+.el -*- lexical-binding: t; -*-

(use-package irony :ghook 'c-mode-common-hook)
(use-package company-irony :defer t)
(use-package flycheck-irony :defer t
  :init (after+ 'compdef
          (compdef
           :modes 'c-mode-common-hook
           :company '((company-lsp
                       company-irony
                       company-capf
                       company-dabbrev-code)))))

;; does not follow naming convention
(use-package irony-eldoc
  :init (hook+ 'irony-mode-hook
               #'irony-eldoc)
  :defer t)

(after+ 'cc-mode
  (hook+ 'c-mode-common-hook
         '(guess-style-guess-all lsp-mode))
  (general-def :modes '(c-mode c++-mode)
    [tab] #'indent-for-tab-command))

(provide 'cpp+)
;;; cpp+.el ends here
