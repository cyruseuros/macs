;;; gif+.el -*- lexical-binding: t; -*-

(use-package gif-screencast
  :general
  (kbd+
    "gt" '(gif-screencast-toggle-pause :wk "resume gif")
    "gs" '(gif-screencast-stop :wk "stop gif")))

(provide 'gif+)
;;; gif+.el ends here
