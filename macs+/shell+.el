;;; shell+.el -*- lexical-binding: t; -*-

(use-package fish-mode
  :defer t
  :init (setq fish-enable-auto-indent t)
  :config
  (after+ 'compdef
    (compdef
     :modes #'fish-mode
     :capf '(sh-completion-at-point-function)
     :company `(,(append '(company-fish-shell
                           company-shell-env)
                         completion+fallback)))))

(use-package company-shell
  :after sh-script
  :config
  (after+ 'compdef
    (compdef
     :modes #'sh-mode
     :capf '(sh-completion-at-point-function)
     :company `(,(append '(company-shell
                           company-shell-env)
                         completion+fallback)))))

(list+ 'interpreter-mode-alist
       '("execlineb" . sh-mode))

(provide 'shell+)
;;; shell+.el ends here
