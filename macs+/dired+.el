;;; dired+.el -*- lexical-binding: t; -*-

;;; vars
(setq dired+ranger nil
      wm+0-command #'dired-sidebar-jump-to-sidebar)

;;; funs
(defun dired+ nil
  (interactive)
  (dired default-directory))

(defun dired+sidebar-winum-0 nil
  (when
      (string-suffix-p
       (buffer-name)
       (dired-sidebar-buffer-name
        (buffer-name)))
    0))

;;; packages
(use-package dired
  :straight nil
  :defer t
  :general (kbd+ "od" #'dired+)
  (kbd+local
    :keymaps 'dired-mode-map
    kbd+localleader #'wdired-finish-edit
    "c" #'wdired-abort-changes
    "a" #'org-attach-dired-to-subtree)
  (general-def :states 'normal "-" #'dired+)
  :init (setq dired-listing-switches
              "-lah --group-directories-first")
  :config
  (when dired+ranger
    (general-def
      :keymaps 'dired-mode-map
      :states 'normal
      "h" #'dired-up-directory
      "l" #'dired-find-file)))

(use-package dired-sidebar
  :init
  (setq dired-sidebar-theme 'ascii
        dired-sidebar-width 40
        dired-sidebar-use-term-integration t
        dired-sidebar-no-delete-other-windows t)
  :general
  (general-def
    :states '(normal insert emacs)
    :keymaps 'dired-sidebar-mode-map
    "-" #'dired-sidebar-up-directory)
  (kbd+
    "0" wm+0-command
    "ft" #'(dired-sidebar-toggle-with-current-directory
            :wk "toggle filetree"))
  :config
  (dired-async-mode +1)
  (after+ 'winum
    (list+ 'winum-assign-functions
           #'dired+sidebar-winum-0)))

(use-package diredfl
  :ghook 'dired-mode-hook)
(use-package all-the-icons-dired
  :config
  (add-to-list
   'all-the-icons-icon-alist
   '("^dockerfile" all-the-icons-fileicon
     "dockerfile" :face all-the-icons-blue))
  :ghook 'dired-mode-hook)
(use-package dired-hacks-utils
  :config (dired-utils-format-information-line-mode)
  :after dired)

(provide 'dired+)
;;; dired+.el ends here
