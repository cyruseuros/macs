;;; rice+.el -*- lexical-binding: t; -*-

;;; vars
(setq rice+theme 'ewal-doom-vibrant
      rice+font-family "Source Code Pro"
      rice+font (font-spec
                 :family rice+font-family
                 :weight 'semi-bold
                 :size 10.5)
      rice+reload-server t
      rice+reload-transient t
      frame-resize-pixelwise t)

;;; funs
(defun rice+clean nil
  (setq inhibit-splash-screen t
        inhibit-startup-message t
        initial-scratch-message nil)

  (when (and
         (fboundp #'scroll-bar-mode)
         (fboundp #'tool-bar-mode)
         (fboundp #'menu-bar-mode))
    (scroll-bar-mode -1)
    (tool-bar-mode -1)
    (menu-bar-mode -1))
  (show-paren-mode +1)
  (global-hl-line-mode +1)
  (tooltip-mode -1))

(defun rice+vborder nil
  (let ((background
         (ewal-get-color 'background 0)))
    (rice+
     `(vertical-border
       :background ,background
       :foreground ,background))))

(defun rice+org nil
  (rice+ `(org-table :inherit 'org-block))
  (rice+ `(org-superstar-leading
           :foreground (ewal-get-color 'comment))))

(defun rice+outline nil
  (let ((background (ewal-get-color 'background -1))
        (background-dark (ewal-get-color 'background -2)))
    (rice+
     `(outline-1 :background ,background-dark)
     `(outline-2 :background ,background)
     `(outline-3 :background ,background)
     `(outline-4 :background ,background)
     `(outline-5 :background ,background)
     `(outline-6 :background ,background)
     `(outline-7 :background ,background)
     `(outline-8 :background ,background))))

(defun rice+theme nil
  (when rice+theme (load-theme rice+theme t)))

(defun rice+font nil
  (set-frame-font rice+font t)
  (list+ 'default-frame-alist
         `(font . ,(font-xlfd-name rice+font))))

(defun rice+modeline nil
  (when (featurep 'doom-modeline)
    (setq doom-modeline-icon (display-graphic-p))))

(defun rice+reload (&optional frame)
  (interactive)
  ;; apply to current frame
  (after+ 'highlight-indent-guides
    (highlight-indent-guides-auto-set-faces))
  (when frame (select-frame frame))
  (rice+modeline)
  (rice+vborder)
  (rice+outline)
  (rice+clean)
  (rice+theme)
  (rice+font)
  (rice+org))

;;; packages
(use-package ewal-doom-themes
  :config
  (defalias 'rice+
    (apply-partially
     #'doom-themes-set-faces 'user))
  :init (setq rice+theme 'ewal-doom-vibrant)
  :straight (ewal-doom-themes
             :type git
             :host gitlab
             :repo "jjzmajic/ewal"
             :branch "develop"
             :files ("doom-themes/*.el")))

(use-package doom-modeline
  :init (doom-modeline-mode +1))
(use-package rainbow-mode :defer t)

;;; calls
(rice+reload)
(kbd+ "rt" '(rice+reload :wk "reload theme"))
(when (and rice+reload-server (daemonp))
  (hook+ 'after-make-frame-functions
         #'rice+reload nil nil
         rice+reload-transient))

;; too jarring to set later
(when (member 'progrice+ init+modules)
  (setq display-line-numbers-type 'relative)
  (global-display-line-numbers-mode +1))

(provide 'rice+)
;;; rice+.el ends here
