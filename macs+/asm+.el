;;; asm+.el -*- lexical-binding: t; -*-

(use-package nasm-mode
  :defer t)

(provide 'asm+)
;;; asm+.el ends here
