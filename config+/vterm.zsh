vterm_printf() {
    if [[ -n "$TMUX" ]]; then
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [[ "${TERM%%-*}" = "screen" ]]; then
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}
alias clear='vterm_printf "51;Evterm-clear-scrollback";tput clear'
setopt promptsubst
vterm_prompt_end() { vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"; }
PROMPT=$PROMPT'%{$(vterm_prompt_end)%}'
